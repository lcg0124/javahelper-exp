package com.bootdo.javahelper;

/**
 * @author lichunguang
 * @since v1.0
 */
public interface Helper {
    void initPackage(String path, String name);

    void addClass(String packag, String name);

    void addMethod(Class re, String methodName, Class param);
}
