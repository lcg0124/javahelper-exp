package com.bootdo.javahelper;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class DefaultHelper implements Helper {
    @Override
    public void initPackage(String path, String name) {
        String projectPath = System.getProperty("user.dir");
        //TODO path前缀处理
        projectPath = projectPath + File.separator + "src/main/java" + File.separator + path.replace(".", "/");
        File pFile = new File(projectPath);
        if (!pFile.exists()) {
            pFile.mkdir();
        }

        List<String> packs = Arrays.asList("controller", "service", "model", "dao");

        for (String pack : packs) {
            File file = new File(projectPath + File.separator + pack);
            if (!file.exists()) {
                file.mkdirs();
                File fileClazz = new File(projectPath + File.separator + pack + File.separator + name + pack + ".java");
                if (!fileClazz.exists()) {
                    try {
                        fileClazz.createNewFile();
                        OutputStream os = new FileOutputStream(projectPath + File.separator + pack + File.separator + name + pack + ".java");
                        PrintWriter pw = new PrintWriter(os);
                        pw.println("package " + path + "." + pack+";");
                        pw.println();
                        pw.println("public class " + name + pack + " {");
                        pw.println("}");
                        pw.close();
                        os.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void addClass(String packag, String name) {
        initPackage(packag, name);
    }

    @Override
    public void addMethod(Class re, String methodName, Class param) {

    }
}
